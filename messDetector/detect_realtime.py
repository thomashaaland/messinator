import numpy as np
import torch
import torchvision
from torchvision.models import detection
import torchvision.transforms as tranforms
import torch.nn as nn
import cv2
import torch.nn.functional as F
from utils import preprocess
import imutils

class PersonFinder:
    """
    A class for detecting people in particular.
    It is based on the Coco dataset and detects people in an image
    """
    def __init__(self, imageDims):
        self.CLASSES = ["person"]
        self.STATUS = self.CLASSES[0]
        self.width = imageDims["width"]
        self.height = imageDims["height"]
        
        self.device = torch.device('cuda')
        print("Hello from PersonPredictor")
        
        with open("coco_classes.txt") as file:
            CLASSES = file.read().split('\n')

        self.model = detection.retinanet_resnet50_fpn(pretrained=True, progress=True,
                        num_classes=len(CLASSES), pretrained_backbone=True).to(self.device)
      
        self.model.eval()
        
                
    def predict(self, img):
        """
        The predict method finds people and makes a triangledraws rectangles around people in the image img.
        @params: img
            img is an image for which the prediction should take place.
        @return: confidences: list, boxes: list
            returns two lists containing confidence values and coordinates for the
            corresponding bounding boxes.
        """
        frame = img
        width = frame.shape[1]
        height = frame.shape[0]
        print("Width, height: ", width, height)
        orig = frame.copy()

        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        frame = frame.transpose((2, 0, 1))

        frame = np.expand_dims(frame, axis=0)
        frame = frame / 255.0
        frame = torch.FloatTensor(frame)

        frame = frame.to(self.device)
        detections = self.model(frame)[0]
        
        boxes = []
        confidences = []

        for i in range(0, len(detections["boxes"])):
            confidence = detections["scores"][i]
            if confidence > 0.5:
                idx = int(detections["labels"][i])
                if idx == 1:
                    box = detections["boxes"][i].detach().cpu().numpy()
                    print(box)
                    boxes.append(box)
                    labels.append("person")
                    confidences.append(confidence)
                    
        return confidences, boxes
