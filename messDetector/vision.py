import cv2
import numpy as np

class Vision:
    def __init__(self, image, classes):
        self.Width = image.shape[1]
        self.Height = image.shape[0]
        scale = 0.00392
        self.color = np.array([0, 255, 255])
        self.net = cv2.dnn.readNet("yolov3-tiny.weights", "yolov3.cfg")
        blob = cv2.dnn.blobFromImage(image, scale, (416, 416), (0,0,0), True, crop=False)
        self.net.setInput(blob)

    def get_output_layers(self, net):
        layer_names = self.net.getLayerNames()
        output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
        return output_layers

    def draw_bounding_box(self, img, class_id, confidence, x, y, x_plus_w, y_plus_h):
        label = str(classes[class_id])
        cv2.rectangle(img, (x,y), (x_plus_w, y_plus_h), color, 2)
        cv2.putText(img, label, (x-10, y-10), cv2.FONT_HERSEY_SIMPLEX, 0.5, color, 2)
        
    def vision(self, image):        
        outs = self.net.forward(self.get_output_layers(self.net))
        
        class_ids = []
        confidences = []
        boxes = []
        conf_threshold = 0.5
        nms_threshold = 0.4
        
        for out in outs:
            for detection in out:
                scores = detection[5:]
                class_id = np.argmax(scores)
                confidence = scores[class_id]
                if confidence > 0.5:
                    center_x = int(detection[0] * self.Width)
                    center_y = int(detection[1] * self.Height)
                    w = int(detection[2] * self.Width)
                    h = int(detection[3] * self.Height)
                    
                    x = center_x - w / 2
                    y = center_y - h / 2
                    class_ids.append(class_id)
                    confidences.append(float(confidence))
                    boxes.append([x, y, w, h])
                    
        indices = cv2.dnn.NMSBoxes(boxes, confidences, conf_threshold, nms_threshold)
        
        for i in indices:
            i = i[0]
            box = boxes[i]
            x = box[0]
            y = box[1]
            w = box[2]
            h = box[3]
            
            draw_bounding_box(image, class_ids[i], confidences[i], round(x), round(y), round(x+w), round(y+h))
        
        return image
