import numpy as np
import torch
import torchvision
from torchvision.models import detection
import torchvision.transforms as tranforms
import torch.nn as nn
import cv2
import torch.nn.functional as F
from utils import preprocess

# Need to modify the base neural network. The NeuralNetwork class
# creates one outputlayer consisting of two fully connected layers.
# These two layers are responsible for holding confidence values (labels)
# and boundingboxes (box)
class NeuralNetwork(nn.Module):
  def __init__(self, in_feat, out_feat):
    super(NeuralNetwork, self).__init__()
    self.linear1 = torch.nn.Linear(in_features = in_feat, out_features = out_feat[0])
    self.linear2 = torch.nn.Linear(in_features = in_feat, out_features = out_feat[1])

  def forward(self, x):
    output1 = self.linear1(x)
    output2 = self.linear2(x)
    return {"labels": output1, "box": output2}

class Predictor:
    """
    The Predictor class is designed to work with pretrained networks.
    The network needs to have the output layer from NeuralNetwork above"""
    def __init__(self, imageDims, preTrainedModel, classes):
        """
        Constructor
        @params:
            imageDims: dictionary
                dictionary with width and height of original image dimensions
            preTrainedModel: string
                path to a pretrained model
            classes: list
                list with the names of the different classes in the pretrained model
        """
        self.CLASSES = classes
        self.STATUS = self.CLASSES[1]
        self.width = imageDims["width"]
        self.height = imageDims["height"]
        
        device = torch.device('cuda')
        print("Hello from Predictor")
        
        cats = len(self.CLASSES)
        output_dim = 4 * cats  # x1, y1, x2, y2 coordinate for each category

        # RESNET 18 Multi Output
        self.model = torchvision.models.resnet18(pretrained=True)
        self.model.fc = NeuralNetwork(512, [cats, output_dim])

        self.model = self.model.to(device)
        
        # Change model if desirable here
        self.model = self.model.to(device)
        self.model.load_state_dict(torch.load(preTrainedModel))
        self.model.eval()
        
                
    def predict(self, img):
        """
        Predict method is responsible for performing the prediction on an image
        @params: img
            img is an image to perform the prediction on.
        @return: finalCat: string, finalConfidence: float, finalCoords: list[int]
        """
        preprocessed = preprocess(img)
        #preprocessed = img
        width = img.shape[0]
        height = img.shape[1]
        outputs = self.model(preprocessed)
        
        box = outputs["box"].detach().cpu().numpy().flatten()
        label = outputs["labels"]
        confidence = F.softmax(label, dim=-1).detach().cpu().numpy().flatten()
        
        for idx, cat in enumerate(self.CLASSES):
            if confidence[idx] > 0.5:
                self.STATUS = cat
                x1 = box[0 + idx*4]
                y1 = box[1 + idx*4]
                x2 = box[2 + idx*4]
                y2 = box[3 + idx*4]

                x1 = int(self.width * (x1 / 2 + 0.5))
                y1 = int(self.height * (y1 / 2 + 0.5))
                x2 = int(self.width * (x2 / 2 + 0.5))
                y2 = int(self.height * (y2 / 2 + 0.5))
                
                finalCat = cat
                finalConfidence = confidence[idx]
                finalCoords = (x1, y1, x2, y2)
        
        return finalCat, finalConfidence, finalCoords        
    
def main():
    #camera = Camera.instance(width=300, height=300)
    print("Initiated camera")
    # Take picture
    #img = camera.value
    print("Picture taken")
    
    pred = Predictor(img)
    print("Succeeded in making predictor")
    pred.predict(dog)
    print("Succeeded in predicting")
    camera.stop()
    
if __name__ == "__main__":
    main()