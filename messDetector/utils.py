# Copyright 2020 NVIDIA
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import torch
import torchvision.transforms as transforms
import torch.nn.functional as F
import cv2
import PIL.Image
import numpy as np

mean = torch.Tensor([0.485, 0.456, 0.406]).cuda()
std = torch.Tensor([0.229, 0.224, 0.225]).cuda()

def preprocess(image):
    device = torch.device('cuda')
    image = PIL.Image.fromarray(image)
    image = transforms.Resize(size = (224, 224))(image)
    image = transforms.functional.to_tensor(image).to(device)
    image.sub_(mean[:, None, None]).div_(std[:, None, None])
    return image[None, ...]

def makeBBox(img, label, confidence, ulx, uly, lrx, lry, color):
    charSpace = 30
    
    # First we crop the sub-rect from the image
    x1 = ulx
    y1 = uly if uly > int(3*charSpace) else int(3*charSpace)
    x2 = lrx
    y2 = uly + int(1.5*charSpace) if uly > int(3*charSpace) else int(4.5*charSpace)

    sub_img = img[y1:y2, x1:x2]
    black_rect = np.zeros(sub_img.shape, dtype=np.uint8)

    res = cv2.addWeighted(sub_img, 0.5, black_rect, 0.5, 1.0)

    # Putting the image back to its position
    img[y1:y2, x1:x2] = res
    
    #cv2.rectangle(img, (ulx, uly if uly > int(3*charSpace) else int(3*charSpace)), (lrx, uly + int(2*charSpace)), (0,0,0), cv2.FILLED)
    cv2.rectangle(img, (ulx, uly if uly > int(3*charSpace) else int(3*charSpace)), (lrx, lry), color, 2)
    cv2.putText(img, "{}: {:.2f}".format(label, confidence), (ulx, uly + charSpace if uly > 3*charSpace else 3*charSpace + charSpace), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 150, 150), 1)
    return img

def makeStatusLine(img, statusColor, color):
    charSpace = 30
    status = {"green": "No mess Detected", "yellow": "WARNING: mess detected while person present", "red": "ALERT: Mess detected!"}
    cv2.rectangle(img, (0, 0), (img.shape[1], int(2.5*charSpace)), (0,0,0), cv2.FILLED)
    cv2.putText(img, "Status: {}".format(statusColor), (0, charSpace), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 150, 150), 1)
    cv2.putText(img, "{}".format(status[statusColor]), (charSpace, 2*charSpace), cv2.FONT_HERSHEY_SIMPLEX, 1, color, 1)
    return img