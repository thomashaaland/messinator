from torchvision.models import detection
from imutils.video import VideoStream
from imutils.video import FPS
import numpy as np
import argparse
import imutils
import torch
import time
import cv2

ap = argparse.ArgumentParser()
ap.add_argument("-m", "--model", type=str, default="frcnn-mobilenet",
    choices=["frcnn-resnet", "frcnn-mobilenet", "retinanet"],
    help="name of the object detection model")
ap.add_argument("-l", "--labels", type=str, default="coco_classes.txt",
	help="path to file containing list of categories in COCO dataset")
ap.add_argument("-c", "--confidence", type=float, default=0.5,
	help="minimum probability to filter weak detections")
args = vars(ap.parse_args())

DEVICE = torch.device("cuda")

with open(args["labels"]) as file:
    CLASSES = file.read().split('\n')
COLORS = np.random.uniform(0,255, size=(len(CLASSES), 3))

MODELS = {
    "frcnn-resnet": detection.fasterrcnn_resnet50_fpn,
	"frcnn-mobilenet": detection.fasterrcnn_mobilenet_v3_large_320_fpn,
	"retinanet": detection.retinanet_resnet50_fpn
}

model = MODELS[args["model"]] (pretrained=True, progress=True,
    num_classes=len(CLASSES), pretrained_backbone=True).to(DEVICE)
model.eval()

print("[INFO] starting video stream...")
vs = VideoStream(src=0).start()
time.sleep(2.0)
fps = FPS().start()

while True:
    frame = vs.read()
    frame = imutils.resize(frame, width = 800)
    orig = frame.copy()

    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    frame = frame.transpose((2, 0, 1))

    frame = np.expand_dims(frame, axis=0)
    frame = frame / 255.0
    frame = torch.FloatTensor(frame)

    frame = frame.to(DEVICE)
    detections = model(frame)[0]

    for i in range(0, len(detections["boxes"])):
        confidence = detections["scores"][i]

        if confidence > args["confidence"]:
            idx = int(detections["labels"][i])
            box = detections["boxes"][i].detach().cpu().numpy()
            (startX, startY, endX, endY) = box.astype("int")

            label = "{}: {:.2f}%".format(CLASSES[idx], confidence * 100)
            cv2.rectangle(orig, (startX, startY), (endX, endY), COLORS[idx], 2)
            y = startY - 15 if startY - 15 > 15 else startY + 15
            cv2.putText(orig, label, (startX, y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[idx], 2)

    cv2.imshow("Frame", orig)
    key = cv2.waitKey(1) & 0xFF # q quits

    if key == ord("q"):
        break

    fps.update()

fps.stop()
print("[INFO] elapsed time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

cv2.destroyAllWindows()
vs.stop()
