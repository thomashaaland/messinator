class StatusResolver:
    """
    The status resolver is a logic gate which returns a color status 
    dependent on three states.
    """
    def __init__(self, greenStatus, yellowStatus, redStatus):
        self.green = greenStatus
        self.yellow = yellowStatus
        self.red = redStatus
        
    def resolveStatus(self, stat1, stat2):
        if stat1 in self.green and stat2 in self.green:
            return "green"
        if stat1 in self.yellow and stat2 in self.yellow:
            return "yellow"
        else:
            return "red"