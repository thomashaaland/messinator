# Copyright 2020 NVIDIA
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
from robotcontrol import RobotControl
from flask import Flask, Response, render_template, request
import time
from jetbot import Camera
import numpy as np


# Initiating flask
app = Flask(__name__,
            template_folder='templates')

# Initiating necessary variables. Need to be global due to Flask
camera = None
camera_dims = {"height": 840, "width": 1024}

rob = None

#Setting up the camera and initiating
def init():
    global camera
    global rob
    print("[INFO]- Init Camera")
    camera = Camera.instance(width=camera_dims["width"], height=camera_dims["height"])
    print("[INFO]- Taking picture")
    img = camera.value
    print("[INFO]- Picture taken")
    rob = RobotControl(camera_dims, img)
    print("[INFO]- Initialized")
    

#######Moving functions that can be called by sending a request to the route url######
@app.route("/indexx")
def root():
    return render_template('index.html')

###################################################################################
#Camera stream function
@app.route("/camera")
def camera():
    return Response(gen(), mimetype='multipart/x-mixed-replace; boundary=frame')

def gen():
    """
    Generator
    Event Loop. Performs prediction, sets status, initiates robot tracking and yields modified frame
    """
    while True:
        time.sleep(0.1)
        frameOut = np.ascontiguousarray(camera.value.copy())
                
        frameOut = rob.respondToFrame(frameOut)
                
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frameOut + b'\r\n\r\n')

    
def main():
    #Starting the app
    init()
    app.run(debug=True, host='0.0.0.0', use_reloader = False)
    #resetting the robot after application has been exited with CTRL+C
    rob.stop()
    if camera is not None:
        camera.stop()
        camera = None # Release the camera

if __name__ == "__main__":
    main()