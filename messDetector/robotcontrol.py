# Copyright 2020 NVIDIA
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from jetbot import Robot
from SCSCtrl import TTLServo
import time
import cv2 as cv
import pred
from utils import preprocess, makeBBox, makeStatusLine
from jetbot import bgr8_to_jpeg
from statusResolver import StatusResolver as sr
import numpy as np

class RobotControl:
    def __init__(self, cam_dims, img):
        self.servoPos_4 = 0
        self.servoPos_1 = 0  # Pan position of the camera.
        self.servoPos_5 = 0  # The vertical position of the camera.

        self.redCountDown = 30

        self.greenStatus = ["person", "no person", "clean"]
        self.yellowStatus = ["person", "clutter"]
        self.redStatus = ["no person", "clutter"]
        self.color = {"green": (0, 255, 0), "yellow": (0, 255, 255), "red": (0, 0, 255)}

        self.status = "track_mess"

        self.personClasses = ['person', 'no person']
        self.clutterClasses = ['clutter', 'clean']
        self.camera_dims = cam_dims

        self.statusResolver = sr(self.greenStatus, self.yellowStatus, self.redStatus)

        print("[INFO]- Setting up mess detector")
        self.messPredictor = pred.Predictor(self.camera_dims, "messofob.pth", self.clutterClasses)
        print("[INFO]- Setting up person detector")
        self.personPredictor = pred.Predictor(self.camera_dims, "personDetect.pth", self.personClasses)
        print("[INFO]- Initial Prediction")
        self.messPredictor.predict(img)
        self.personPredictor.predict(img)

        self.robot = Robot()
        TTLServo.servoAngleCtrl(1, 0, 1, 150)
        TTLServo.servoAngleCtrl(2, 0, 1, 150)
        TTLServo.servoAngleCtrl(3, 0, 1, 150)
        TTLServo.servoAngleCtrl(4, 0, 1, 150)
        TTLServo.servoAngleCtrl(5, 0, 1, 150)
        self.jiggle()

    def jiggle(self):
        TTLServo.servoAngleCtrl(1, 10, 1, 150)
        time.sleep(1)
        TTLServo.servoAngleCtrl(2, 10, 1, 150)
        time.sleep(1)
        TTLServo.servoAngleCtrl(3, 10, 1, 150)
        time.sleep(1)
        TTLServo.servoAngleCtrl(4, 10, 1, 150)
        time.sleep(1)
        TTLServo.servoAngleCtrl(5, 10, 1, 150)
        time.sleep(1)
        TTLServo.servoAngleCtrl(1, -10, 1, 150)
        time.sleep(1)
        TTLServo.servoAngleCtrl(2, -10, 1, 150)
        time.sleep(1)
        TTLServo.servoAngleCtrl(3, -10, 1, 150)
        time.sleep(1)
        TTLServo.servoAngleCtrl(4, -10, 1, 150)
        time.sleep(1)
        TTLServo.servoAngleCtrl(5, -10, 1, 150)
        time.sleep(1)
        self.neutral()
    
    def neutral(self):
        TTLServo.servoAngleCtrl(1, 0, 1, 150)
        TTLServo.servoAngleCtrl(2, 0, 1, 150)
        TTLServo.servoAngleCtrl(3, 0, 1, 150)
        TTLServo.servoAngleCtrl(4, 0, 1, 150)
        TTLServo.servoAngleCtrl(5, 30, 1, 150)
    
    def limitCtl(self, maxInput, minInput, rawInput):
        if rawInput > maxInput:
            limitBuffer = maxInput
        elif rawInput < minInput:
            limitBuffer = minInput
        else:
            limitBuffer = rawInput

        return limitBuffer

    def calcX(self, angleDeg):
        angle = (angleDeg) * np.pi / 180
        y = -150
        x = y * np.tan(angle)
        print(x)
        return x if x > 85 else 85

    ################ Increment camera Panning ############
    def cameraUp(self):
        self.servoPos_5 = self.limitCtl(25, -40, self.servoPos_5-15)
        TTLServo.servoAngleCtrl(5, self.servoPos_5, 1, 150)

    def cameraDown(self):
        self.servoPos_5 = self.limitCtl(25, -40, self.servoPos_5+15)
        TTLServo.servoAngleCtrl(5, self.servoPos_5, 1, 150)

    # Camera turn right
    def ptRight(self):
        self.servoPos_1 = self.limitCtl(80, -80, self.servoPos_1+15)
        TTLServo.servoAngleCtrl(1, self.servoPos_1, 1, 150)

    # Camera turn left
    def ptLeft(self):
        self.servoPos_1 = self.limitCtl(80, -80, self.servoPos_1-15)
        TTLServo.servoAngleCtrl(1, self.servoPos_1, 1, 150)
    
    ################ Variable speed camera Panning #######
    # camera looks up.
    def cameraUpVar(self, speedInput):
        servoCtrlTime = 0.001
        TTLServo.servoAngleCtrl(5, -70, 1, speedInput)
        time.sleep(servoCtrlTime)

    # camera looks down.
    def cameraDownVar(self, speedInput):
        servoCtrlTime = 0.001
        TTLServo.servoAngleCtrl(5, 30, 1, speedInput)
        #TTLServo.servoAngleCtrl(5, 45, 1, speedInput)
        time.sleep(servoCtrlTime)

    # camera looks right.
    def ptRightVar(self, speedInput):
        servoCtrlTime = 0.001
        TTLServo.servoAngleCtrl(1, 80, 1, speedInput)
        time.sleep(servoCtrlTime)

    # camera looks left.
    def ptLeftVar(self, speedInput):
        servoCtrlTime = 0.001
        TTLServo.servoAngleCtrl(1, -80, 1, speedInput)
        time.sleep(servoCtrlTime)

    # camera tilt axis motion stops.
    def tiltStop(self):
        servoCtrlTime = 0.001
        TTLServo.servoStop(5)
        time.sleep(servoCtrlTime)

    # camera pan axis motion stops.
    def panStop(self):
        servoCtrlTime = 0.001
        TTLServo.servoStop(1)
        time.sleep(servoCtrlTime)
        
    def stop(self):
        self.robot.stop()
    
    def step_forward(self):
        self.robot.forward(0.9)

    def step_backward(self):
        self.robot.backward(0.9)

    def step_left(self):
        self.robot.left(0.9)

    def step_right(self):
        self.robot.right(0.9)

    def xIn(self):
        self.xPos += 15
        TTLServo.xyInput(self.xPos, self.yPos)

    def xDe(self):
        self.xPos -= 15
        if self.xPos < 85:
            self.xPos = 85
        TTLServo.xyInput(self.xPos, self.yPos)

    def yIn(self):
        self.yPos += 15
        TTLServo.xyInput(self.xPos, self.yPos)

    def yDe(self):
        self.yPos -= 15
        TTLServo.xyInput(self.xPos, self.yPos)

    def grab(self):
        self.servoPos_4 -= 15
        if self.servoPos_4 < -90:
            self.servoPos_4 = -90
        TTLServo.servoAngleCtrl(4, self.servoPos_4, 1, 150)

    def loose(self):
        self.servoPos_4+= 15
        if self.servoPos_4 > -10:
            self.servoPos_4 = -10
        TTLServo.servoAngleCtrl(4, self.servoPos_4, 1, 150)
    
    def off(self):
        TTLServo.servoAngleCtrl(1, 0, 1, 150)
        TTLServo.servoAngleCtrl(2, 0, 1, 150)
        TTLServo.servoAngleCtrl(3, 0, 1, 150)
        TTLServo.servoAngleCtrl(4, 0, 1, 150)
        TTLServo.servoAngleCtrl(5, 0, 1, 150)
    
    ######### Method for tracking with camera #########
    def cameraTrack(self, width: int, height: int, x: int, y: int):
        error_tor = 25
        PID_P = 1

        midHeight = int(height/2)
        midWidth = int(width/2)

        X = int(x)
        Y = int(y)

        error_Y = abs(midHeight - Y)
        error_X = abs(midWidth - X)

        if Y < (midHeight - error_tor):
            # Camera looks up.
            self.cameraUpVar(error_Y*PID_P)
        elif Y > (midHeight + error_tor):
            # Camera looks down.
            self.cameraDownVar(error_Y*PID_P)
        else:
            self.tiltStop()

        if X < (midWidth - error_tor):
            # Camera looks left.
            self.ptLeftVar(error_X*PID_P)
        elif X > (midWidth + error_tor):
            # Camera looks right.
            self.ptRightVar(error_X*PID_P)
        else:
            self.panStop()
        
    ######### Different postures
    def redPosture(self):
        self.xPos = 200
        self.yPos = -50
        TTLServo.xyInput(self.xPos, self.yPos)

    def yellowPosture(self):
        self.xPos = 85
        self.yPos = 150
        TTLServo.xyInput(self.xPos, self.yPos)

    def greenPosture(self):
        self.xPos = 85
        self.yPos = 100
        TTLServo.xyInput(self.xPos, self.yPos)
    

    ########## Response behaviour ###############
    def respondToFrame(self, frameOut):
        if self.status == "track_mess":
            return self.respondTrack(frameOut)
        elif self.status == "clean":
            return self.respondClean(frameOut)
    

    def respondTrack(self, frameOut):
        messStatus, messConfidence, messCoords = self.messPredictor.predict(frameOut)
        personStatus, personConfidence, personCoords = self.personPredictor.predict(frameOut)    
    
        statusColor = self.statusResolver.resolveStatus(personStatus, messStatus)
        currentColor = self.color[statusColor]

        frameOut = makeBBox(frameOut, messStatus, messConfidence, *messCoords, currentColor)
        frameOut = makeBBox(frameOut, personStatus, personConfidence, *personCoords, currentColor)
        frameOut = makeStatusLine(frameOut, statusColor, currentColor)

        frameOut = bgr8_to_jpeg(frameOut)

        if statusColor == "red" or statusColor == "yellow":
            x = int((messCoords[0] + messCoords[2])/2)
            y = int((messCoords[1] + messCoords[3])/2)
            cameraTrack(camera_dims["width"], camera_dims["height"], x, y)
            if statusColor == "red":
                #self.redCountDown -= 1
                if self.redCountDown <= 0:
                    self.status = "clean"
                self.redPosture()
            else:
                self.yellowPosture()
        else:
            self.neutral()
            self.greenPosture()

        return frameOut

    def respondClean(self, frameOut):
        self.pickUpItem()
        frameOut = bgr8_to_jpeg(frameOut)
        return frameOut

    def pickUpItem(self):
        #range: closest ground: x = 100, y = -150
        angle = self.servoPos_5
        print(angle)
        x = self.calcX(angle)
        print(x)
        self.xPos = 110
        self.yPos = -150
        TTLServo.xyInput(self.xPos, self.yPos)