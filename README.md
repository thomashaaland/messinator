# Messinator

Turning a JetTank into a mess tracker.

## Intro

Anyone with children know about the eternal struggle to keep the house tidy. 
The Messinator robot keeps track of unattended mess and is intended to warn 
the user if mess is left alone. 

The robot is based on a JetTank and uses Nvidia Jetson Nano to perform realtime 
mess detection.

## Contents

*   [What is this?](#what-is-this)
*   [Getting started](#getting-started)
    *   [Requirements](#requirements)
    *   [Install](#install)
    *   [Usage](#usage)
*   [Project description](#project-description)
    * [Used Technologies](#used-technologies)
    * [Testing](#testing)
*   [Contribute](#contribute)
*   [License](#license)
*   [Conclusion](#conclusion)

## What is this?

This project is intended for homeowners with children. Using this robot they can track 
whether a mess has been made and if that mess is left unattended. If it is, the robot 
signals by lowering its arm and points at the mess. 

## Getting Started

Follow instrictons here to get started with the Messinator project. 

To get started you need to . Once it is assembled you can follow the [install](#install) instructions to make it come alive and detect mess in your home.

### Requirements

* Assembled [JetTank](https://www.waveshare.com/jetank-ai-kit.htm) from [Waveshare](https://www.waveshare.com/)
* Knowhow to follow [tutorials](https://www.youtube.com/watch?v=qNy1hulFk6I&t=1263s) for assembling robots

### Install

* Download the 4GB Jetson Nano image from [jetbot.org](https://jetbot.org/master/software_setup/sd_card.html) 
* Etch it to an SD card using [Balena Etcher](https://www.balena.io/etcher/)
* Once the robot is assembled and booted with the SD card, log in and activate WiFi.
* Download JETANK using `sudo git clone https://github.com/waveshare/JETANK.git`

Now install using 

```
cd JETANK/
sudo chmod +x config.sh
sudo chmod +x install.sh
./config.sh jetbot
```

The device will reboot. Now you can log on using a webbrowser using the IP address displayed on the back of the robot and port 8888, password 'jetbot'. Next install JETANK

```
cd JETANK
./install.sh
```

Next, from the Jupyter Lab window accessed from port 8888, you also need to download and install the Jetson repository. 

```
git clone https://github.com/NVIDIA-AI-IOT/jetcam
cd jetcam
sudo python3 setup.py install
```

Now you can get this repository using

```
https://gitlab.com/thomashaaland/messinator.git
```

Install the requirements in [requirements.txt](requirements.txt) using `pip3 install requirements.txt`.

### Usage

From root run `messDetector/flask_server.py` to start the robot. Then use a webbrowser to log in on port 
`<ip>:5000/camera`. You should be able to see what the Messinator sees. If you present the robot with
duplo, it should recognise it as 'mess'. Also, it will track the duplo, but be in yellow mode while there 
is a person in it's field of view. If there is no person visible it will point to the mess in addition to 
tracking it. Finally, if there is no mess visible it will be in standby.

You can also retrain the robot by running the `makeModel/regression_interactive.ipynb` file in jupyter. 
If you make a new model you can save it as `messDetector/messofob.pth` to identify the mess, and 
`messDetector/personDetect.pth` to detect what it would react to as a person.

## Project description

The project attempts to identify mess. I have trained it specifically to go after Duplo as mess. 
Currently the robot has three 'modes' running dependently on what it sees. If it see no 'mess' it 
is in resting mode with a green gui. If it sees 'mess' while there is a person present it will track 
the mess and keep it's arm pointed away from the person with a yellow gui. However, if the person leaves
and there is only mess present it will point at the 'mess' and the gui will turn red.

![greenGui](images/messinatorGreenGui.PNG)

![yellowGui](images/messinatorYellowGui.PNG)

![redGui](images/messinatorRedGui.PNG)

### Used technologies

* Jetson Nano
* JetTank

### Testing

To see how it behaves, present the robot with different objects. It should primarily react to Duplo. 
It should also react differently with a person visible as opposed to hidden.

[Demonstration on youtube](https://youtu.be/0QpYasu9QLE)


## Contribute

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[Nvidia](http://www.apache.org/licenses/LICENSE-2.0)

## Conclusion

We have here a robot which will react to a mess being made with duplo. It can be retrained to react to different objects.
